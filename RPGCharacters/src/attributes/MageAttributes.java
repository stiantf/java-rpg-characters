package attributes;

import items.WeaponType;

public class MageAttributes extends CharacterAttributes{
    WeaponType[] weaponType;

    //Mages basestats
    public Attributes setBaseAttributes() {
        return new Attributes(5,1,1,8);
    }

    //Mages levelup stats
    public Attributes levelUpAttributes(Attributes attributes) {
        attributes.updateAttributes(new Attributes(3,1,1,5));
        return attributes;
    }

    //Mages weapontypes:
    public WeaponType[] getWeaponsType() {
       return weaponType = new WeaponType[]{WeaponType.Axes, WeaponType.Wands};
    }

}
