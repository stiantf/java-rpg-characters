package characters;

public enum CharacterType {
    Mage,
    Ranger,
    Rogue,
    Warrior
}
