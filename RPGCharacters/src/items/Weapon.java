package items;

import characters.CharacterType;

public class Weapon extends Items {
    WeaponType axe = WeaponType.Axes;
    WeaponType bow = WeaponType.Bows;
    WeaponType dagger = WeaponType.Daggers;
    WeaponType hammer = WeaponType.Hammers;
    WeaponType staff = WeaponType.Staffs;
    WeaponType sword = WeaponType.Swords;
    WeaponType wand = WeaponType.Wands;

    WeaponType weaponType;
    int damage;
    int attackSpeed;


    public Weapon(String name, int requiredLevel, Slots slot, WeaponType weaponType, int damage, int attackSpeed) {
        super(name, requiredLevel, slot);
        this.weaponType = weaponType;
        this.damage = damage;
        this.attackSpeed = attackSpeed;
    }


    //Getters and setters
    public WeaponType getWeaponType() {
        return weaponType;
    }
    public void setWeaponType(WeaponType weaponType) {
        this.weaponType = weaponType;
    }
    public int getDamage() {
        return damage;
    }
    public void setDamage(int damage) {
        this.damage = damage;
    }
    public int getAttackSpeed() {
        return attackSpeed;
    }
    public void setAttackSpeed(int attackSpeed) {
        this.attackSpeed = attackSpeed;
    }


}
