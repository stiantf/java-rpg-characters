package characters;

import attributes.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CharactersTest {

    @Test
    void getLevel_NewCharacter_IsLevel1() {
        //Arrange
        String name = "Character";
        Characters character = new Characters(name, CharacterType.Mage, new MageAttributes());
        int expected = 1;
        //Act
        int actual = character.getLevel();
        //Assert
        assertEquals(expected, actual);
    }

    @Test
    void levelUp_CharacterIsLevel1_ShouldBecomeLevel2() {
        //Arrange
        String name = "Character";
        Characters character = new Characters(name, CharacterType.Mage, new MageAttributes());
        int expected = 2;
        //Act
        int actual = character.levelUp();
        //Assert
        assertEquals(expected,actual);
    }

    @Test
    void CreatingNewMage_Level1_CorrectBaseAttributes() {
        //Arrange
        String name = "mage";
        Characters character = new Characters(name, CharacterType.Mage, new MageAttributes());
        String expected = "vitality="+5+" strength="+1+" dexterity="+1+" intelligence="+8;
        //Act
        String actual = character.attributes.getAttributes();
        //Assert
        assertEquals(expected,actual);
    }

    @Test
    void CreatingNewRanger_Level1_CorrectBaseAttributes() {
        //Arrange
        String name = "ranger";
        Characters character = new Characters(name, CharacterType.Ranger, new RangerAttributes());
        String expected = "vitality="+8+" strength="+1+" dexterity="+7+" intelligence="+1;
        //Act
        String actual = character.attributes.getAttributes();
        //Assert
        assertEquals(expected,actual);
    }

    @Test
    void CreatingNewRogue_Level1_CorrectBaseAttributes() {
        //Arrange
        String name = "rogue";
        Characters character = new Characters(name, CharacterType.Rogue, new RogueAttributes());
        String expected = "vitality="+8+" strength="+2+" dexterity="+6+" intelligence="+1;
        //Act
        String actual = character.attributes.getAttributes();
        //Assert
        assertEquals(expected,actual);
    }

    @Test
    void CreatingNewWarrior_Level1_CorrectBaseAttributes() {
        //Arrange
        String name = "warrior";
        Characters character = new Characters(name, CharacterType.Warrior, new WarriorAttributes());
        String expected = "vitality="+10+" strength="+5+" dexterity="+2+" intelligence="+1;
        //Act
        String actual = character.attributes.getAttributes();
        //Assert
        assertEquals(expected,actual);
    }

    @Test
    void levelUpAttributes_MageAttributesUpdatesAtLevelUp_CorrectLevelUpAttributes() {
        //Arrange
        String name = "mage";
        Characters character = new Characters(name, CharacterType.Mage, new MageAttributes());
        character.levelUp();
        String expected = "vitality="+8+" strength="+2+" dexterity="+2+" intelligence="+13;
        //Act
        String actual = character.attributes.getAttributes();
        //Assert
        assertEquals(expected,actual);
    }

    @Test
    void levelUpAttributes_RangerAttributesUpdatesAtLevelUp_CorrectLevelUpAttributes() {
        //Arrange
        String name = "ranger";
        Characters character = new Characters(name, CharacterType.Ranger, new RangerAttributes());
        character.levelUp();
        String expected = "vitality="+10+" strength="+2+" dexterity="+12+" intelligence="+2;
        //Act
        String actual = character.attributes.getAttributes();
        //Assert
        assertEquals(expected,actual);
    }

    @Test
    void levelUpAttributes_RogueAttributesUpdatesAtLevelUp_CorrectLevelUpAttributes() {
        //Arrange
        String name = "rogue";
        Characters character = new Characters(name, CharacterType.Rogue, new RogueAttributes());
        character.levelUp();
        String expected = "vitality="+11+" strength="+3+" dexterity="+10+" intelligence="+2;
        //Act
        String actual = character.attributes.getAttributes();
        //Assert
        assertEquals(expected,actual);
    }

    @Test
    void levelUpAttributes_WarriorAttributesUpdatesAtLevelUp_CorrectLevelUpAttributes() {
        //Arrange
        String name = "warrior";
        Characters character = new Characters(name, CharacterType.Warrior, new WarriorAttributes());
        character.levelUp();
        String expected = "vitality="+15+" strength="+8+" dexterity="+4+" intelligence="+2;
        //Act
        String actual = character.attributes.getAttributes();
        //Assert
        assertEquals(expected,actual);
    }
}