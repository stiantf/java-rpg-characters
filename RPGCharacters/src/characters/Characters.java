package characters;

import attributes.Attributes;
import attributes.CharacterAttributes;
import items.Weapon;
import items.WeaponType;

import java.util.Arrays;

public class Characters {

    String name;
    int level = 1;
    CharacterType characterType;
    public Attributes attributes;
    CharacterAttributes CharacterAttributes;
    WeaponType[] weaponTypes;
    private String InvalidWeaponException;

    //Constructor
    public Characters(String name, CharacterType characterType, CharacterAttributes characterAttributes) {
        this.name = name;
        this.characterType = characterType;
        CharacterAttributes = characterAttributes;
        setBaseStats();
        setWeaponTypes();
    }

    //Setting the characters base stats:
    private void setBaseStats() {
        attributes = CharacterAttributes.setBaseAttributes();
    }

    //Setting the characters weapon types
    private void setWeaponTypes() {weaponTypes = CharacterAttributes.getWeaponsType();}


    //Character levelup:
    public int levelUp() {
        level++;
        attributes = CharacterAttributes.levelUpAttributes(attributes);
        return level;
    }

    //Showing the characters name, type, level and attributes
    public String printStats() {
        return name + " The " + characterType + " is level " + level +
                "\n" +
                name + "'s stats: " + attributes.getAttributes();
        //Should also print dps
    }

    //Getter to get character level
    public int getLevel() {
        return level;
    }


    //Check if the weapon can be equipped by this character
    public Object canEquipWeapon(Weapon weapon) {
        //check for level requirements and correct weapontypes:
            if (level >= weapon.getRequiredLevel() && Arrays.asList(weaponTypes).contains(weapon.getWeaponType()) ) {
                System.out.println("Weapon can be equipped");
                //Weapon can be equipped -> updating dps
                return true;
            } return new Exception(InvalidWeaponException);
    }






}
