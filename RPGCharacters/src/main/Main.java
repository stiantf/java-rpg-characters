package main;

import attributes.MageAttributes;
import attributes.RangerAttributes;
import attributes.RogueAttributes;
import attributes.WarriorAttributes;
import characters.CharacterType;
import characters.Characters;
import items.Slots;
import items.Weapon;
import items.WeaponType;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws Exception {
        boolean endGame = false;
        Scanner scan = new Scanner(System.in);

        //Starting the game. Getting info from user

        System.out.println("Game has started. Please enter a name:");
        String name = scan.nextLine();
        System.out.println("Now choose your character");
        System.out.println("Enter 1 for Mage");
        System.out.println("Enter 2 for Ranger");
        System.out.println("Enter 3 for Rogue");
        System.out.println("Enter 4 for Warrior");

        //Setting the characters type based on user input
        int type = Integer.parseInt(scan.nextLine());
        //String character = "";
        Characters character;
        switch (type) {
            //creates the character object
            case 1:character = new Characters(name, CharacterType.Mage, new MageAttributes()); break;
            case 2:character = new Characters(name, CharacterType.Ranger, new RangerAttributes()); break;
            case 3:character = new Characters(name, CharacterType.Rogue, new RogueAttributes()); break;
            case 4:character = new Characters(name, CharacterType.Warrior, new WarriorAttributes());
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + type);
        }

        //Menu for the user to choose different options
        while(endGame == false) {
            System.out.println(" ");
            System.out.println("----M E N U ----");
            System.out.println("Enter 1 to show character stats");
            System.out.println("Enter 2 to level up character");
            System.out.println("Enter 3 to equip a weapon");
            System.out.println("Press 9 to exit");

            String input = scan.nextLine();

            //If the user enters 1 show character info and attributes
            if(Integer.parseInt(input) == 1) {
                System.out.println(character.printStats());
            }

            //If the user enters 2 the character levels up
            if(Integer.parseInt(input) == 2) {
                character.levelUp();
                System.out.println(character.printStats());
            }

            //If the user enters 3 show first a list of available weapons:
            if(Integer.parseInt(input) == 3) {
                System.out.println("Select 1 for Axe");
                System.out.println("Select 2 for Bow");
                System.out.println("Select 3 for Dagger");
                System.out.println("Select 4 for Hammer");
                System.out.println("Select 5 for Staff");
                System.out.println("Select 6 for Sword");
                System.out.println("Select 7 for Wand");
                int weaponChoice = Integer.parseInt(scan.nextLine());
                Weapon weapon;
                switch (weaponChoice) {
                    case 1: weapon = new Weapon("Axe", 1, Slots.Weapon, WeaponType.Axes, 1, 1); break;
                    case 2: weapon = new Weapon("Bow", 1, Slots.Weapon, WeaponType.Bows, 1, 1); break;
                    case 3: weapon = new Weapon("Dagger", 1, Slots.Weapon, WeaponType.Daggers, 1, 1); break;
                    case 4: weapon = new Weapon("Hammer", 1, Slots.Weapon, WeaponType.Hammers, 1, 1); break;
                    case 5: weapon = new Weapon("Staff", 1, Slots.Weapon, WeaponType.Staffs, 1, 1); break;
                    case 6: weapon = new Weapon("Sword", 1, Slots.Weapon, WeaponType.Swords, 1, 1); break;
                    case 7: weapon = new Weapon("Wand", 1, Slots.Weapon, WeaponType.Wands, 1, 1); break;
                    default:
                        throw new IllegalStateException("Unexpected value: " + type);
                }
                //Check if the character can equip the selected item
                character.canEquipWeapon(weapon);

            }

            //If the user enters 9 the game stops
            if(Integer.parseInt(input) == 9) {
                System.out.println("Game has ended!");
                endGame = true;
            }
        }

    }
}
