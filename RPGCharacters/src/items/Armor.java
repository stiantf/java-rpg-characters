package items;

import attributes.Attributes;

public class Armor extends Items {
    ArmorType cloth = ArmorType.Cloth;
    ArmorType leather = ArmorType.Leather;
    ArmorType mail = ArmorType.Mail;
    ArmorType plate = ArmorType.Plate;

    ArmorType armorType;
    public Attributes attributes;



    public Armor(String name, int requiredLevel, Slots slot) {
        super(name, requiredLevel, slot);
    }



    public void setAttributes(Attributes attribute) {
        attributes = attribute;
    }

}
