package items;

public abstract class Items {
    String name;
    int requiredLevel;
    Slots slot;

    public Items(String name, int requiredLevel, Slots slot) {
        this.name = name;
        this.requiredLevel = requiredLevel;
        this.slot = slot;
    }

    //Getters and setters
    public String getName() {return name;}
    public void setName(String name) {this.name = name;}
    public Slots getSlot() {return slot;}
    public void setSlot(Slots slot) {this.slot = slot;}
    public int getRequiredLevel() {return requiredLevel;}
    public void setRequiredLevel(int requiredLevel) {this.requiredLevel = requiredLevel;}


}
