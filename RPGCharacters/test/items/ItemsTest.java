package items;

import attributes.WarriorAttributes;
import characters.CharacterType;
import characters.Characters;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ItemsTest {

    private String InvalidWeaponException;

    @Test
    void equipWeapon_WeaponLevelToHigh_InvalidWeaponException(){
        //Arrange
        String name = "warrior";
        Characters character = new Characters(name, CharacterType.Warrior, new WarriorAttributes());
        Weapon weapon = new Weapon("Axe", 2, Slots.Weapon, WeaponType.Axes, 1, 1);
        Exception expected = new Exception(InvalidWeaponException);
        //Act
        Object actual = character.canEquipWeapon(weapon);
        //Assert
        assertEquals(expected, actual);
    }

    @Test
    void equipWeapon_WeaponIsWrongType_InvalidWeaponException() {
        //Arrange
        String name = "warrior";
        Characters character = new Characters(name, CharacterType.Warrior, new WarriorAttributes());
        Weapon weapon = new Weapon("Bow", 1, Slots.Weapon, WeaponType.Bows, 1, 1);
        Exception expected = new Exception(InvalidWeaponException);
        //Act
        Object actual = character.canEquipWeapon(weapon);
        //Assert
        assertEquals(expected, actual);
    }

    @Test
    void equipWeapon_CorrectWeaponType_true() {
        //Arrange
        String name = "warrior";
        Characters character = new Characters(name, CharacterType.Warrior, new WarriorAttributes());
        Weapon weapon = new Weapon("Axe", 1, Slots.Weapon, WeaponType.Axes, 1, 1);
        boolean expected = true;
        //Act
        Object actual = character.canEquipWeapon(weapon);
        //Assert
        assertEquals(expected, actual);
    }




}