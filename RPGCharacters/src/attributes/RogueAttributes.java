package attributes;

import items.WeaponType;

public class RogueAttributes extends CharacterAttributes{
    WeaponType[] weaponType;

    //Rogues basestats
    public Attributes setBaseAttributes() {
        return new Attributes(8,2,6,1);
    }

    //Rogues levelup stats
    public Attributes levelUpAttributes(Attributes attributes) {
        attributes.updateAttributes(new Attributes(3,1,4,1));
        return attributes;
    }

    //Rogues weapontypes
    public WeaponType[] getWeaponsType() {
        return weaponType = new WeaponType[]{WeaponType.Daggers, WeaponType.Swords};
    }
}
