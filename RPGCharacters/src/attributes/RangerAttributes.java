package attributes;

import items.WeaponType;

public class RangerAttributes extends CharacterAttributes {
    WeaponType[] weaponType;


    //Rangers basestats
    public Attributes setBaseAttributes() {
        return new Attributes(8, 1, 7, 1);
    }

    //Rangers levelup stats
    public Attributes levelUpAttributes(Attributes attributes) {
        attributes.updateAttributes(new Attributes(2, 1, 5, 1));
        return attributes;
    }

    //Rangers weapontype
    public WeaponType[] getWeaponsType() {
        return weaponType = new WeaponType[]{WeaponType.Bows};
    }


}
