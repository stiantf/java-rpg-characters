package attributes;

import items.WeaponType;

public class WarriorAttributes extends CharacterAttributes{
    WeaponType[] weaponType;

    //warriors basestats
    public Attributes setBaseAttributes() {
        return new Attributes(10,5,2,1);
    }

    // Warriors levelup stats
    public Attributes levelUpAttributes(Attributes attributes) {
        attributes.updateAttributes(new Attributes(5,3,2,1));
        return attributes;
    }

    public WeaponType[] getWeaponsType() {
        return weaponType = new WeaponType[]{WeaponType.Axes, WeaponType.Hammers, WeaponType.Swords};
    }
}
