package attributes;

import items.WeaponType;

public abstract class CharacterAttributes {

    //Sets base attributes based on the character type
    public abstract Attributes setBaseAttributes();

    //attributes change depending on character level.
    public abstract Attributes levelUpAttributes(Attributes attributes);

    public abstract WeaponType[] getWeaponsType();


}
